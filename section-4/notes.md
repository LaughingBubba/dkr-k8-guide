# Making Real Projects with Docker

## Basic Steps
- Create simple node.js App
- Create docker file
- Build image 
- Run image as container
- Connect to App from browser

## Deliberate (but common) Errors
- npm not found: apline >> use node:apline
- ```ENOENT: no such file or directory, open '/package.json'``` >> use:  
COPY <local path relative build context> <internal-container path> 
- Port Mapping (inbound) run time use -p switch <from-external-port>:<port-inside-container>
use:  
```docker run -p 5000:8080 laughingbubba/simpleweb```
- Working App directory use:  
WORKDIR