# Section 12 - Towards Kubernetes

Kubernetes:
- Expects all images to be already built => images need to be hosted somewhere (docker hub / GCR)
- 1x config file per Object => ie: container
- maunually set up networking => Config file for networking

## config file
Used to create objects eg:
- StatefulSet
- ReplicaContainer
- Pod
- Service

### apiVersion
Each API version defines a different set of objects we have access to.

v1:
- componentStatus
- Endpoints
- Namespace
- configMap
- Event
- Pod
- Service

apps/v1:
- ControllerRevision
- StatefulSet

## Node
minkube = node
A node runs 1 or more Pods

## Pods
Pods are the smallest unit of deployment in a node.

Runs one or more closely related containers.

Pods run 1 or more containers. Pods provide a grouping of containers with a similar purpose and must be deployed together for the application to work.

Pod eg:
- Postgres (primary container)
- Logging (support container)
- backup-manager (support container)

## Services
Sets up networking in a k8s Cluster

subtypes:
- ClusterIP 
- NodePort : Exposes container to outside world (for dev only)
- LoadBalancer 
- Ingress 

### Ports
- port: Other Pods can access this Pod via this port (internal)
- targetPort: Inbound port (from outside of node)
- nodePort: exposes (random 30000-32767) to browser (dev)

## running a cluster
```bash
kubectl apply -f <filename>
```

eg (assuming within simplek8s dir):
```bash
kubectl apply -f client-pod.yaml
kubectl apply -f client-node-port.yaml

kubectl get pods
kubectl get services
```

### access running pod
```bash
minikube ip
$> 172.17.0.2
```

Point browser to ```http://172.17.0.2:31515/```

## IMPORTANT Take aways
- K8s is a system to deploy containerised apps
- Nodes are individual machines (or VM's) that run containers
- Masters are machines (or VM's) with a set of programs to manage nodes
- K8s don't build images, they pull them from a Repo / Registry
- K8s Master decides to where to run each container - each node can run a dissimilary set of containers
- **To deploy something, we update the desired state of the Master with a config file**
- **The Master works constantly to meet the desired state**