# Section 15 - Ingress Controllers

LoabBalancer Service = legacy

Ingress = exposes a set of services to the outside world.

NB: We will be using ingress-nginx (community project) NOT
kubernetes-ingress (lead by nginx)

NB: Setup of ingress-nginx changes depending on environment (local, GC, AWS, Azure) we will use local and GC

## Extra reading
https://www.joyfulbikeshedding.com/blog/2018-03-26-studying-the-kubernetes-ingress-system.html.

## Local Ingress
https://kubernetes.github.io/ingress-nginx/deploy/

Has changed from the course.

Added ```ingress-service.yaml``` to ```section-14/complexk8s/k8s```

### Testing
```bash
joseph@Bubuntu:section-14/complexk8s ‹master*›$ minikube ip
172.17.0.2
```

Pint browser to > 172.17.0.2