# Section 5 - Docker Compose

node - redis - visits 

```bash
docker-compose up -d --build
```

## Maintenance
Restart policies: "no" | allways | on-failure | unless-stopped
"no" must be a string yaml.

```bash
docker-compose ps
```