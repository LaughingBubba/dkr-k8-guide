module.exports = {
  redisHost: process.env.REDIS_HOST,
  redisPort: process.env.REDIS_PORT,
  pgUser: process.env.PGUSER,
  pgHost: process.env.PGHOST,
  pgDB: process.env.PGDB,
  pgPWD: process.env.PGPWD,
  pgPort: process.env.PGPORT,
}