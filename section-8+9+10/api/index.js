const keys = require('./keys')
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const PORT = 5000

const app = express()
app.use(cors())
app.use(bodyParser.json())

// Postgress Client Setup
const { Pool } = require('pg')
const pgClient = new Pool({
  user: keys.pgUser,
  host: keys.pgHost,
  database: keys.pgDB,
  password: keys.pgPWD,
  port: keys.pgPort
})

pgClient.on('error', () => {
  console.log(`Lost PG connection`)
})
pgClient
  .query('CREATE TABLE IF NOT EXISTS values (number INT)')
  .catch(err => console.log(`${err}`))

// Redis client set up
const redis = require('redis')
const redisClient = redis.createClient({
  host: keys.redisHost,
  port: keys.redisPort,
  retry_strategy: () => 1000
})
const redisPublisher = redisClient.duplicate()

redisClient.on("error", function(error) {
  console.error(`redisClient error`, error);
})

redisPublisher.on("error", function(error) {
  console.error(`redisPublisher error`, error);
})

// Express route handlers
app.get('/', (req, res) => {
  res.send(`Hi!`)
})

app.get('/values/all', async (req, res) => {
  const values = await pgClient.query('SELECT * from values')
  res.send(values.rows)
})

app.get('/values/current', async (req, res) => {
  console.log(`/values/current EXEC`)
  redisClient.hgetall('values', (err, values) => {
    if (err) {
      console.log(`/values/current error ${err}`)
      return res.status(501).send(err)
    }
    console.log(`/values/current OK`)
    res.send(values)
  })
})

app.post('/values', async (req, res) => {
  const index = req.body.index

  console.log(`Posting index value ${index}`)

  if (parseInt(index) > 40 ) {
    return res.status(422).send('Index too high')
  }

  redisClient.hset('values', index, 'Nothing yet!')
  redisPublisher.publish('insert', index)
  pgClient.query('INSERT INTO values(number) VALUES($1)', [index])

  res.send({ working: true })
})

app.listen(PORT, err => {
  console.log(`Listening on port ${PORT}`)
})