# Build Multi-Container Apps

## Sample App
- Fibonacci Sequence Calculator
- NGINX router to 
  - React App will then
    - Express API
      - Values "I've seen" > Progress
      - Calculated Values > Redis
        - Redis calls a Worker


## Single Container Deployment Issues
- Simple app : no outsode dependencies
- Image build multiple times
- Connect to a DB from a container?

# Section 10 - CI workflow for Multiple Images

## Multi-Container Setup
- Push code to hosted repo
- CI pulls repo
- CI builds Test Image and tests code
- CI builds Prod image
- CI pushes Prod image to Image/Container Repository
- CI Signals to Hosting Service new image available
- Hosting service pulls and deploys new image

PS: I hope this will transalate to GCP as SG switches to GCP from AWS  for Kubernetes section.

## Additional Steps
- Create PROD ready versions of all docker files

# Section 11 - Multi-container deployment to AWS
Skipping as I dont use AWS and am particularly interested in Kubernetes.