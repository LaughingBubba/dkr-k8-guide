# Section 6 - Product Grade Workflow

## Flow
- Develop
- Test
- Deploy

### Github repo
- feature
- master 

branches - Pull request from Feature to Master ->  
Travis CI -> AWS Elastic Beanstalk

## Dev version of Dockerfile
- Dev Dockerfile (Dockerfile.dev)

Use -f option to specify Dockerfile (for other than default)  
```bash
docker build -f Dockerfile.dev .
```

## Volumes
Use docker Volumes for Dev builds for hot code refreshes  
use -v $(pwd)/app

```bash
docker run -it -p 3000:3000 -v $(pwd):/app cb0b176fdc3e
```

nb: pwd = Present Working Directoy
nb: Bookmark volumes -stops over writing from $(pwd):/app
```bash
docker run -it -p 3000:3000 -v /app/node_modules -v $(pwd):/app 1030aff16a07
```

## Running tests
- Add testing service to compose
- But there are limitions to attaching as when running in compose you can only attach to PRIMARY process - testring runs in secondary process

## NGINX - Multistep Docker Builds
- Build Phase
  - Use node:alpine
  - Copy package.json
  - Install Deps
  - Run `npm run build`
- Run Phase
  - Use NGINX
  - Copy result of `npm run build`
  - Start NGINX

```bash
docker build .
docker run -p 5000:80 f9eea1a40df9
```