# Section 3 - Custom Images via Docker Server

## Creating a Dockerfile
Flow:
- Define base image
- Run commands to install additional programs
- Define command to run on container startup

The main exercise for this section will be to build a redis-server from scratch.

Refer the to the Dockerfile in this directory and use
```bash
docker build .
##  ---> 2ed2785078fa
## Successfully built 2ed2785078fa
docker run -d 2ed2785078fa
```

### Tagging an Image
-t <docker-id>/<repo>(or <project>):<version>
```bash
docker build . -t laughingbubba/redis:latest
```

### Manually Creating an Image
```bash
joseph@Bubuntu:dkr-k8-guide/section-3 ‹master*›$ docker run -it alpine sh     
/ # apk add --update redis
fetch http://dl-cdn.alpinelinux.org/alpine/v3.10/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.10/community/x86_64/APKINDEX.tar.gz
(1/1) Installing redis (5.0.5-r0)
Executing redis-5.0.5-r0.pre-install
Executing redis-5.0.5-r0.post-install
Executing busybox-1.30.1-r2.trigger
OK: 7 MiB in 15 packages
/ # 
```

In a separate terminal session:
```bash
joseph@Bubuntu:~ $ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED              STATUS              PORTS               NAMES
07fb4e2ed37a        alpine              "sh"                     About a minute ago   Up About a minute                       awesome_ganguly
9f0dac408a4d        redis               "docker-entrypoint.s…"   3 hours ago          Up 3 hours          6379/tcp            hardcore_hugle
joseph@Bubuntu:~ $ docker commit -cc 'CMD ["redis-server"]' 07fb4e2ed37a
Error response from daemon: No such container: CMD ["redis-server"]
joseph@Bubuntu:~ $ docker commit -c 'CMD ["redis-server"]' 07fb4e2ed37a 
sha256:8b0c47bce608a21d625fe1975004ccc08eb484e9a01c9afd112e44ebb2fa5d7b
joseph@Bubuntu:~ $ docker run 8b0c47bce608a21
1:C 09 Apr 2020 07:58:02.074 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
1:C 09 Apr 2020 07:58:02.074 # Redis version=5.0.5, bits=64, commit=47edd290, modified=0, pid=1, just started
1:C 09 Apr 2020 07:58:02.074 # Warning: no config file specified, using the default config. In order to specify a config file use redis-server /path/to/redis.conf
1:M 09 Apr 2020 07:58:02.075 * Increased maximum number of open files to 10032 (it was originally set to 1024).
1:M 09 Apr 2020 07:58:02.075 * Running mode=standalone, port=6379.
1:M 09 Apr 2020 07:58:02.075 # WARNING: The TCP backlog setting of 511 cannot be enforced because /proc/sys/net/core/somaxconn is set to the lower value of 128.
1:M 09 Apr 2020 07:58:02.075 # Server initialized
1:M 09 Apr 2020 07:58:02.076 # WARNING overcommit_memory is set to 0! Background save may fail under low memory condition. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
1:M 09 Apr 2020 07:58:02.076 # WARNING you have Transparent Huge Pages (THP) support enabled in your kernel. This will create latency and memory usage issues with Redis. To fix this issue run the command 'echo never > /sys/kernel/mm/transparent_hugepage/enabled' as root, and add it to your /etc/rc.local in order to retain the setting after a reboot. Redis must be restarted after THP is disabled.
1:M 09 Apr 2020 07:58:02.076 * Ready to accept connections

```