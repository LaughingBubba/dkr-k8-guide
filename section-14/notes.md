# Section 14 - Multi-container App w Kubernetes


NB: PVC = Persisten Volume Claim

## Path to Production
1. create config file for each service and deployment
2. Test locally on minikube
3. Create CI/CD to build images and deploy
4. Deploy app to cloud provider

## Complexk8 set up
- Clone orginal/complex
- nginx no longer required - replaced by Ingress
- Docker compose no longer required
- Travis CI replaced 

## NodePort vs ClusterIP Service
**ClusterIP:** Exposes a set of pods to other objects in the cluster. No external access.

## Initial Application
Cleanup: (after deleting olde client deployment)
```bash
kubectl get services
kubectl delete service client-node-port
kubectl get services
```

When applying a group of configuration files:
```bash
kubectl apply -f k8s
kubectl get deployments
kubectl get services 
```

```bash
kubectl get pods 
kubectl logs server-deployment-5fdbfcfc99-lg7qg
```

## Combine k8 configs into 1 file
add --- as a config separator

## Volumes and Postgres
PVC = Persistent Volume Claim

Replicas will always be 1 for DB's unless specific clustering is configured.

In container terminology: Volume = mechanism for container to access a file system outside of itself.

In K8s terminology: Volume = **Object** that allows container to store data a Pod level

### K8 Volume Heirarcy
1. Persistent Volume Claim - List of storage option (Static/Dynamic provision)
2. Persistent Volume (not tied to a Pod)
3. Volume at Pod level only (not the same as Docker Volume) 

### Access Modes
ReadWriteOnce = Can only be used by a SINGLE Node 
ReadOnlyMany = MULTIPLE nodes can read from this
ReadWriteMany = Can be read and written to by MANY Nodes

### Allocation of PVs
type of storage class: https://kubernetes.io/docs/concepts/storage/storage-classes

```bash
kubectl get storageclass
kubectl describe storageclass
kubectl get pv
kubectl get pvc
```

### Environment Variables & Secrets
Use env: under container spec

Secrets: Securely stores a piece of information in the cluster. ie: a password

Template:
```bash
kubectl create secret generic <secret_name> --from-literal key=value
```

Example:
```bash
kubectl create secret generic pg-password --from-literal PGPASSWORD=postgres_password

kubectl get secrets
```
Apply updates to config files then:

```bash
kubectl apply -f k8
```


**NOTE:** Will need to be run again in Prod environment