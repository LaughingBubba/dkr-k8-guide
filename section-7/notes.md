# Section 7 - CI CD with AWS
Cloned section 6 and spun off a separate project : https://gitlab.com/LaughingBubba/dkr-k8-guide-proj

## GITLAB / Google Cloud Challenge
Stephen's course assumes Github / Travis-CI / AWS deplyment

where my flow is Gitlab /  Gitlab-CI/CD / Google Cloud Platform (GAE) deployment

So how to transpose? What about storing docker images images?

## Notes
```bash
npm run test -- --coverage
```

- AWS Beanstalk &-| GAE are good for a single container app 

## GCP Steps
- Create new project [dkr-k8-guide]

### References
- simple testing example: https://gitlab.com/gableroux/gitlab-ci-example-nodejs/blob/master/.gitlab-ci.yml  


## Single Container Deployment Issues
- Simple app : no outsode dependencies
- Image build multiple times
- Connect to a DB from a container?
