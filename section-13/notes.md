# Section 13 - Maintaining Sets of Containers with Deppoyments

REMBEMBER: Each Node has it's own copy of docker (internally)

## commands

```bash
kubectl describe pod client-pod

kubectl delete -f client-pod.yaml

kubectl apply -f client-deployment.yaml

kubectl get pods

kubectl get deployments

```

## config file limitations
Not updateable:
- number of containers
- name
- port

Updateable:
- image

Pods are better managed by using the Deployment object type.

## Deployment
Pod Vs Deployment

Pods:
- Runs single set of containers
- Good for one-off Dev purposes
- Rarely used in Prod

Deployment:
- Runs a set of identical pods (one or more)
- Monitors the state of each Pod (updates as necessary)
- Good for Dev AND Prod

MB: STILL DEPENDS on Service object.

## Updating Deployment Images
Imperative command is the leasser of 3 evils

```bash
kubectl set image deployment/client-deployment client=stephengrider/multi-client:v5
```

## Reconfiguring Docker CLI
Note: This is mimilted ot the life of the terminal session (tab)
```bash
eval $(minikube docker-env)
```

Allows you to connect or manipulate containers in the Pod